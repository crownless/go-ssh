package server

import (
	"github.com/spf13/viper"
)

// Config - is server specific options,
// readed from configuration file with viper's help
type Config struct {
	Port int
	Host string
}

// NewServerConfig returns server Config representation
func NewServerConfig() *Config {
	return &Config{
		Port: viper.GetInt("port"),
		Host: viper.GetString("iface"),
	}
}
