package server

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	pathToConfig string
)

// serverRootCommand TODO
var serverRootCommand = &cobra.Command{
	Use:   "server -c /path/to/config.toml",
	Short: "Private ssh key storage",
	Long:  "This server store private ssh keys",
	Run: func(cmd *cobra.Command, args []string) {

	},
}

func parseConfigFile() {
	if pathToConfig == "" {
		_ = serverRootCommand.Help()
		os.Exit(0)
	}
	viper.SetConfigFile(pathToConfig)
	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("Could not read configuration file `%s`, reason: %#v", pathToConfig, err)
		os.Exit(-1)
	}
}

// ParseCliAndCfg todo: Description
func ParseCliAndCfg() error {
	return serverRootCommand.Execute()
}

func init() {
	cobra.OnInitialize(parseConfigFile)
	serverRootCommand.PersistentFlags().StringVarP(&pathToConfig, "path to configuration file", "c", "", "-c /etc/config.toml")

	// viper's flag could be owerriden with cobra's command flag
	serverRootCommand.PersistentFlags().IntP("port", "p", 80, "[-p|--port] 80")
	viper.BindPFlag("port", serverRootCommand.PersistentFlags().Lookup("port"))

	serverRootCommand.PersistentFlags().StringP("iface", "i", "127.0.0.1", "[-i|--iface]  127.0.0.1")
	viper.BindPFlag("iface", serverRootCommand.PersistentFlags().Lookup("iface"))
}
