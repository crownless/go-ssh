package server

import (
	"fmt"
	"net/http"
	"strconv"

	"go.uber.org/zap"
)

// Start igniting the engine and starts to listen incoming
// http requests.
func Start(cfg *Config, assets map[string][]byte) {

	fmt.Println(assets)

	http.HandleFunc("/", Index)
	http.HandleFunc("/static/css/main.css", GetCss)

	addr := cfg.Host + ":" + strconv.Itoa(cfg.Port)
	Logger.Info("Start web server on port", zap.String("addr", addr))

	if err := http.ListenAndServe(addr, nil); err != nil {
		Logger.Fatal("Failed to start server", zap.Error(err))
	}
}
