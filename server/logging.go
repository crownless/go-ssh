package server

import (
	"encoding/json"
	"fmt"
	"os"

	"go.uber.org/zap"
)

// Logger is a base logger for whole server application
var Logger *zap.Logger

func init() {
	rawJSONConfig := []byte(`{
		"level": "debug",
		"encoding": "json",
		"outputPaths": ["stdout"],
		"errorOutputPaths": ["stderr"],
		"encoderConfig": {
		  "messageKey": "message",
		  "levelKey": "level",
		  "levelEncoder": "lowercase"
		}
	  }`)

	var cfg zap.Config
	if err := json.Unmarshal(rawJSONConfig, &cfg); err != nil {
		fmt.Printf("Malformed logger config. eason: %#v", err)
		os.Exit(1)
	}
	logger, err := cfg.Build()
	if err != nil {
		fmt.Printf("Could not innitialize logger. Reason: %#v", err)
	}
	Logger = logger
	defer Logger.Sync()
}
