package server

import (
	"fmt"
	"html/template"
	"net/http"
)

var INDEX_PAGE string = `
<html>
<head>
	<title>
		public ssh keys watcher
	</title>
	<link rel="stylesheet" href="/static/css/main.css">
</head>
<body>
	da!
</body>
</html>
`

// Index is the main we handler
func Index(w http.ResponseWriter, r *http.Request) {
	tmpl := template.New("index")
	tmpl.Parse(INDEX_PAGE)
	data := map[string]string{"pew": "pew"}
	tmpl.Execute(w, data)
}

func GetCss(w http.ResponseWriter, r *http.Request) {
	fmt.Println(r.RequestURI)
	w.Header()["content-type"] = []string{"text/css"}
	fmt.Println("got hit!")
	fmt.Fprint(w, []byte("background-color: red;"))
	return
}
