FROM golang:latest
LABEL Author="crwnl3ss<metalgearangel@gmail.com>"

RUN mkdir -p ~/go/src/ssh_key_observer/
WORKDIR ~/go/src/ssh_key_observer/
COPY . ~/go/src/ssh_key_observer/
