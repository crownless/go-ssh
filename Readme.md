#### Development server environment:

install & build bindata utility: go build -o go-bindata.exe github.com/jteeuwen/go-bindata/go-bindata
link assets recursive
go-bindata.exe ./server/web/...
cp ./bindata.go ./cmd/server/