package main

import (
	"errors"
	"fmt"

	"github.com/crwnl3ss/ssh/server"
)

func prepareAssets() (map[string][]byte, error) {
	assets := make(map[string][]byte)
	indx, err := Asset("assets/templates/index.html")
	if err != nil {
		return nil, errors.New("Index page asset not found")
	}
	assets["index"] = indx
	return assets, nil
}

func main() {
	if err := server.ParseCliAndCfg(); err != nil {
		fmt.Println("Could not parse cli args")
	}
	cfg := server.NewServerConfig()
	assets, err := prepareAssets()
	if err != nil {
		fmt.Println(":*")
	}
	server.Start(cfg, assets)
}
