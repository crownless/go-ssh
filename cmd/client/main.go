package main

import (
	"fmt"

	"github.com/crwnl3ss/ssh/client"
)

func main() {
	if err := client.ExecuteCommand(); err != nil {
		fmt.Println(":(")
	}
}
