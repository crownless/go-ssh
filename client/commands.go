package client

import (
	"fmt"

	"github.com/spf13/cobra"
)

var rootCommand = &cobra.Command{
	Use:   "pew",
	Short: "to short",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("pew-pew", args)
	},
}

// ExecuteCommand ...
func ExecuteCommand() error {
	return rootCommand.Execute()
}
